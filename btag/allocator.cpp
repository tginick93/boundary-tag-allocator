#include "allocator.hpp"

namespace BoundaryTag
{
	Arena::Arena()
	{

	}

	Arena::~Arena()
	{
		// free everything
		char* blk = reinterpret_cast<char*>(_pRootBlock);
		delete[] blk;
	}

	static inline constexpr uint64_t additionalSpaceRequired()
	{
		return 2 * sizeof(uint64_t) + sizeof(void*);
	}

	Arena* Arena::makeArena(uint64_t totalArenaSize)
	{
		Arena* na = new Arena();

		CoalescingBlock* ncb = CoalescingBlock::makeBlock(totalArenaSize);

		na->_pFreeList = ncb;
		na->_pFreeListTail = ncb;
		na->_pRootBlock = ncb;
		na->_originalSize = totalArenaSize;

		return na;
	}

	void* Arena::allocate(uint64_t numBytesRequested)
	{
		// make sure the current freelist block has enough space before trying to allocate
		// numBytesRequested + 24 bytes required!
		CoalescingBlock* currentPtr = _pFreeList;
		CoalescingBlock* prevPtr = nullptr;

		while (currentPtr != nullptr)
		{
			std::cout << "Curr BS: " << currentPtr->getBlockSize() << ". Needed: " << numBytesRequested + additionalSpaceRequired() << '\n';

			if (currentPtr->getBlockSize() < numBytesRequested + additionalSpaceRequired())
			{
				prevPtr = currentPtr;
				currentPtr = currentPtr->getNextPtr();
			}
			else
			{
				break;
			}
		}

		if (currentPtr == nullptr)
		{
            std::cout << "Could not find available block\n";
			return nullptr;
		}

		CoalescingBlock* nb = CoalescingBlock::splitBlock(currentPtr, numBytesRequested);

		return reinterpret_cast<void*>(nb->getDataSection());
	}

	void Arena::free(void* memory)
	{
		// pointer to block is 16 above memory pointer
		char* blk = reinterpret_cast<char*>(memory) - sizeof(void*) - sizeof(uint64_t);
		CoalescingBlock* realBlk = reinterpret_cast<CoalescingBlock*>(blk);
		realBlk->markBlockAsFree(); // really just sets the next ptr to nullptr

        // take the current free list's tail and appends the freed block to it
        CoalescingBlock* previousFreeListTail = _pFreeListTail; // needed for coalescing upwards
        
		_pFreeListTail->updateFreePtr(realBlk);
		_pFreeListTail = realBlk;

        // not sure why i thought this the first time...but we need to coalesce DOWN FIRST
        while (true)
        {
            char* nextBlk = blk + realBlk->getBlockSize() + additionalSpaceRequired();
            CoalescingBlock* realNextBlk = reinterpret_cast<CoalescingBlock*>(nextBlk);
            
            // first make sure this pointer is in range...
            if (nextBlk >= reinterpret_cast<char*>(_pRootBlock) + _originalSize)
            {
                // ...not in range. break
                std::cout << "> Next Block not in range\n";
                break;
            }

            if (realNextBlk->getNextPtr() == nullptr)
            {
                // this next block is also free so let's coalesce it
                std::cout << "> Down\n";
                CoalescingBlock* coalescedResult = CoalescingBlock::coalesceSecond(realBlk, realNextBlk);

                // this is all we need to do
                (void) coalescedResult;
            }
            else if (realNextBlk->getNextPtr() != reinterpret_cast<CoalescingBlock*>(0x1))
            {
                // any other case that is not "in use"
                // need to traverse free list (or introduce prev ptr) to find the next pointer that points to this block
                // this is because when coalesced, that pointer needs to be updated to point to the resultant block
                // in coalescing down, the resultant block will have an address less than the specified next pointer
                
            }
            else
            {
                // this block is not free. should break
                std::cout << "> Next Block is not free: " << reinterpret_cast<uint64_t>(realNextBlk->getNextPtr()) << "\n";
                break;
            }
        }

        // now let's go up
        // consider removing some of these checks for speed?
        while (true)
        {
            uint64_t* pPrevTrailingBlkSize = reinterpret_cast<uint64_t*>(blk - sizeof(uint64_t));

            // is this pointer valid? 
            if (pPrevTrailingBlkSize < reinterpret_cast<uint64_t*>(_pRootBlock))
            {
                // this ptr is before the root block
                std::cout << "> Prev Block trailer is not in range\n";
                break;
            }

            uint64_t prevTrailingBlkSize = *pPrevTrailingBlkSize;

            char* prevBlk = blk - additionalSpaceRequired() - prevTrailingBlkSize;
            CoalescingBlock* realPrevBlk = reinterpret_cast<CoalescingBlock*>(prevBlk);
            
            //
            if (prevBlk < reinterpret_cast<char*>(_pRootBlock))
            {
                // this ptr is before the root block
                std::cout << "> Prev Block is not in range\n";
                break;
            }

            if (realPrevBlk->getNextPtr() == nullptr)
            {
                std::cout << "> Up\n";
                
                CoalescingBlock* coalescedResult = CoalescingBlock::coalesceSecond(realPrevBlk, realBlk);

                // we need to update the tail's next pointer
                previousFreeListTail->updateFreePtr(coalescedResult);
                _pFreeListTail = coalescedResult;
            }
            else if (realPrevBlk->getNextPtr() != reinterpret_cast<CoalescingBlock*>(0x1))
            {
                
            }
            else
            {
                // previous block is not free
                std::cout << "> Prev Block is not free: " << reinterpret_cast<uint64_t>(realPrevBlk->getNextPtr()) << "\n";
                break;
            }
        }
	}

	/*
		off: 	data: 		
		0		bs
		8		nextptr
		16		data
		16 + ds bs
	*/
	CoalescingBlock* CoalescingBlock::makeBlock(uint64_t totalBlockSize)
	{
		static_assert(sizeof(void*) == 8, "Sorry, only 64 bit compilation suppported at the moment.");

		static const uint64_t _off0 = 0;
		(void) _off0;

		static const uint64_t _off1 = sizeof(void*); // sizeof uint64_t + alignment;
		static const uint64_t _offData = sizeof(void*) + _off1; // sizeof uint64_t + sizeof pointer. 8 on 32bit, 12 on 64bit
	
		char* sink = new char[totalBlockSize];

		uint64_t usableBlockSize = totalBlockSize - (_offData + _off1);
		uint64_t _offEnd = _offData + usableBlockSize;

		*(reinterpret_cast<uint64_t*>(sink)) = usableBlockSize;
		*(reinterpret_cast<CoalescingBlock**>(sink + _off1)) = nullptr;
		//*(reinterpret_cast<char**>(sink + _offData)) = sink + _offData;
		*(reinterpret_cast<uint64_t*>(sink + _offEnd)) = usableBlockSize;

		return reinterpret_cast<CoalescingBlock*>(sink);
	}

	CoalescingBlock* CoalescingBlock::splitBlock(CoalescingBlock* oldBlock, uint64_t requestedNewBlockSize)
	{
		uint64_t actualBlockSize = requestedNewBlockSize + additionalSpaceRequired();

		oldBlock->_bs -= actualBlockSize;
		uint64_t* oldTrailing = oldBlock->getTrailingBlockSizePtr();
		*oldTrailing = oldBlock->_bs;

        CoalescingBlock* newBlockStart = reinterpret_cast<CoalescingBlock*>(reinterpret_cast<char*>(oldTrailing) + sizeof(uint64_t));
		newBlockStart->_bs = requestedNewBlockSize;
		newBlockStart->_next = reinterpret_cast<CoalescingBlock*>(0x1);

		uint64_t* newTrailing = newBlockStart->getTrailingBlockSizePtr();
		*newTrailing = newBlockStart->_bs;

		return newBlockStart;
	}

	// takes up and combines it with down. does no error checking so make sure up and down are actually free space first
	CoalescingBlock* CoalescingBlock::coalesceSecond(CoalescingBlock* up, CoalescingBlock* down)
	{
		up->_bs += down->_bs + additionalSpaceRequired();

		uint64_t* pNewTrailing = up->getTrailingBlockSizePtr();
		*pNewTrailing = up->_bs;

		return up;
	}

	void CoalescingBlock::updateFreePtr(CoalescingBlock* nf)
	{
		CoalescingBlock** pNextPtr = &_next;
		*pNextPtr = nf;
	}

	void CoalescingBlock::markBlockAsFree()
	{
		CoalescingBlock** pNextPtr = &_next;
		*pNextPtr = nullptr;
	}
}
