#pragma once

#include <cstdint>
#include <vector>

#include <iostream>

namespace BoundaryTag
{
	struct SmallTestStruct
	{
		uint64_t ifield;
		char a;
		char b;
		char c;
		char d;
	}; // sizeof = 12

	// only Arena should really be working with this class.
	class CoalescingBlock
	{
	public:
		CoalescingBlock() = delete;

	public:
		uint64_t getBlockSize() const { return _bs; }
		CoalescingBlock* getNextPtr() const { return _next; }

		char* getDataSection() const 
		{ 
			return const_cast<char*>(reinterpret_cast<const char*>(this) + sizeof(void*) + sizeof(uint64_t));
		}

		uint64_t getTrailingBlockSize() const 
		{ 
			return *(reinterpret_cast<uint64_t*>(getDataSection() + _bs)); 
		}

		uint64_t* getTrailingBlockSizePtr() const
		{
			return reinterpret_cast<uint64_t*>(getDataSection() + _bs);
		}

	public:
		static CoalescingBlock* makeBlock(uint64_t totalBlockSize);
		static CoalescingBlock* splitBlock(CoalescingBlock* oldBlock, uint64_t requestedNewBlockSize);
		static CoalescingBlock* coalesceSecond(CoalescingBlock* first, CoalescingBlock* second);
		
	public:
		void markBlockAsFree();
		void updateFreePtr(CoalescingBlock* nf);

	private:
		uint64_t _bs;
		CoalescingBlock* _next;
	};


	class Arena
	{
	public:
		~Arena();
		
	private:
		Arena();

	public:
		void* allocate(uint64_t numBytesRequested);
		void free(void* memory);
		
	public:
		static Arena* makeArena(uint64_t totalArenaSize);

	private:
		CoalescingBlock* _pRootBlock;
		CoalescingBlock* _pFreeList;
		CoalescingBlock* _pFreeListTail;
		uint64_t _originalSize;
	};
}
