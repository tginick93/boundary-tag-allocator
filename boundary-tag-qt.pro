TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11

HEADERS += btag/allocator.hpp
SOURCES += main.cpp btag/allocator.cpp

