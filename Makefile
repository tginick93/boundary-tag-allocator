CC = clang++
CFLAGS = -Wall -c -std=c++11
LDFLAGS =
TARGET = btag.linux
CFILES = main.cpp btag/allocator.cpp

COBJS = $(CFILES:.cpp=.o)

all: $(TARGET)

$(TARGET): $(COBJS)
	$(CC) $(COBJS) -o $(TARGET) $(LDFLAGS)

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm *.o $(TARGET)
