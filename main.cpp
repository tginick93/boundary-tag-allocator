#include <iostream>

#include "btag/allocator.hpp"

int main(int argc, char** argv)
{
    (void) argc;
    (void) argv;

    std::cout << "Boundary allocator test harness\n";

    BoundaryTag::Arena* na = BoundaryTag::Arena::makeArena(1024);

    std::cout << "Alloc 1\n";
    BoundaryTag::SmallTestStruct* ns1 = reinterpret_cast<BoundaryTag::SmallTestStruct*>(na->allocate(sizeof(BoundaryTag::SmallTestStruct)));
    ns1->ifield = 128;
    ns1->a = 'h';
 	ns1->b = 'e';
 	ns1->c = 'l';
 	ns1->d = '\0';

 	std::cout << ns1->ifield << ", " << &ns1->a << std::endl;

 	std::cout << "Free 1\n";
 	na->free(ns1);

    std::cout << "Alloc 2a\n";
 	BoundaryTag::SmallTestStruct* ns2 = reinterpret_cast<BoundaryTag::SmallTestStruct*>(na->allocate(sizeof(BoundaryTag::SmallTestStruct)));
    ns2->ifield = 129;
    ns2->a = 'a';
 	ns2->b = 'g';
 	ns2->c = 'a';
 	ns2->d = '\0';

    std::cout << "Alloc 2b\n";
 	BoundaryTag::SmallTestStruct* ns3 = reinterpret_cast<BoundaryTag::SmallTestStruct*>(na->allocate(sizeof(BoundaryTag::SmallTestStruct)));
    ns3->ifield = 130;
    ns3->a = 'l';
 	ns3->b = 'o';
 	ns3->c = 'l';
 	ns3->d = '\0';

 	std::cout << ns2->ifield << ", " << &ns2->a << std::endl;
 	std::cout << ns3->ifield << ", " << &ns3->a << std::endl;

 	std::cout << "Free 1a\n";
 	na->free(ns2);
 	std::cout << ns3->ifield << ", " << &ns3->a << std::endl;

 	std::cout << "Free 1b\n";
 	na->free(ns3);

    std::cout << "Alloc 3\n";
    BoundaryTag::SmallTestStruct* ns4 = reinterpret_cast<BoundaryTag::SmallTestStruct*>(na->allocate(sizeof(BoundaryTag::SmallTestStruct)));
    ns4->ifield = 140;
    ns4->a = 'y';
    ns4->b = 'a';
    ns4->c = 'y';
    ns4->d = '\0';

    std::cout << ns4->ifield << ", " << &ns4->a << std::endl;

    std::cout << "Free 3\n";
    na->free(ns4);
    
 	std::cout << "Shutdown\n";
    delete na;

    return 0;

}
